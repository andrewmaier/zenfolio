local socket = require "socket"
local http = require "socket.http"
local ltn12 = require "ltn12"
local ssl = require "ssl"

local params = {
    mode = "client",
    protocol = "sslv23",
    cafile = "/etc/ssl/certs/ca-certificates.crt",
    verify = "peer",
    options = "all",
}

local try = socket.try
local protect = socket.protect

function create()
    local t = {c=try(socket.tcp())}

    function idx (tbl, key)
        print("idx " .. key)
        return function (prxy, ...)
                   local c = prxy.c
                   return c[key](c,...)
               end
    end


    function t:connect(host, port)
        print ("proxy connect ", host, port)
        try(self.c:connect(host, port))
        print ("connected")
        self.c = try(ssl.wrap(self.c,params))
        print("wrapped")
        try(self.c:dohandshake())
        print("handshaked")
        return 1
    end

    return setmetatable(t, {__index = idx})
end

local response_body = {}


b, c, h = http.request{
    url = "https://www.example.com:443/",
    sink = ltn12.sink.table(response_body),
    create = create,
}

print(b, c, h)

print(table.concat(response_body))
